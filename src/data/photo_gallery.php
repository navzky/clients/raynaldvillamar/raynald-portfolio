<?php

$photoAlbum = array(
    (object) array("name" => "wwwroot/images/img01.jpg", "caption" => "1st Birthday", "description" => "My first Birthday", "date" => "07/03/2020"),
    (object) array("name" => "wwwroot/images/img02.jpg", "caption" => "Where I met them", "description" => "Something Happen", "date" => "07/03/2016"),
    (object) array("name" => "wwwroot/images/img03.jpg", "caption" => "Where I met you", "description" => "Something Happen", "date" => "07/03/2021")
);

usort($photoAlbum, function($first, $second) { 
    return strtotime($first->date) > strtotime($second->date);
});

echo json_encode($photoAlbum);

?>