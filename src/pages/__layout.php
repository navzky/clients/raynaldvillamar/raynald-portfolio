<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/wwwroot/css/main.css" />
    <title>Raynald Villamar</title>
</head>
<body>
    <nav>
        <a href="#personalInformationSection">Personal Information</a>
        <a href="#hobbiesSection">Hobbies</a>
        <a href="#portfolioSection">Portfolio</a>
        <a href="#gallerySection">Gallery</a>
    </nav>
    <section id="personalInformationSection"><?php include_once 'personal_information.php'; ?></section>
    <section id="hobbiesSection"><?php include_once 'hobbies.php'; ?></section>
    <section id="portfolioSection"><?php include_once 'portfolio.php'; ?></section>
    <section id="gallerySection"><?php include_once 'gallery.php'; ?></section>
    

    <script type="text/javascript" src="/wwwroot/js/jquery.min.js"></script>
    <script type="text/javascript" src="/wwwroot/js/polaroid-gallery.js"></script>
    <script type="text/javascript" src="/wwwroot/js/main.js"></script>
</body>
</html>